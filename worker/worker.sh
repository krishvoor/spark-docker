#!/bin/sh

echo "Initiating the worker node"
export SPARK_HOME=/opt/spark

. "${SPARK_HOME}/sbin/spark-config.sh"

. "${SPARK_HOME}/bin/load-spark-env.sh"

bin/spark-class org.apache.spark.deploy.worker.Worker \
    --webui-port $SPARK_WORKER_WEBUI_PORT -c $SPARK_WORKER_CORES -m $SPARK_WORKER_MEMORY $SPARK_MASTER >> $SPARK_MASTER_LOG/spark-worker.out
